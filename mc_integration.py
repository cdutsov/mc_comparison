from random import seed
from random import random
import sys
import math

x_a = 0  # lower limit for X values
x_b = math.pi  # upper limit for X values
y_a = 0  # lower limit for Y values
y_b = 1  # upper limit for Y values

count = 0
n = 0
seed(1)

if len(sys.argv) > 1:
    n = int(sys.argv[1])
else:
    exit(1)

for i in range(0, n):
    x = random()*x_b + x_a
    y = random()*y_b + y_a
    if y < math.sin(x):
        count += 1

count_ratio = float(count)/float(n)
area = x_b*y_b
integral_value = count_ratio*area

print("Total iterations:", n)
print("Values under the curve:", count)
print("Area:", area)
print("Integral value:", integral_value)
