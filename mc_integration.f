        program integral
            implicit none

            real::PI = DACOS(-1.D0)
            real::x_a = 0.d0
            real::x_b = 0.d0
            real::y_a = 0.d0
            real::y_b = 1.d0
            real::x = 0.d0
            real::y = 0.d0
            real::cr = 0.d0
            real::a = 0.d0
            real::iv = 0.d0
            integer::c = 0
            integer::n = 100000000
            integer::i = 0

            x_b = PI

            do i=1,n
                x = rand() * x_b + x_a
                y = rand() * y_b + y_a

                if (y .lt. sin(x)) then
                    c = c + 1
                endif
            enddo

            cr = c / real(n, 16)
            a = x_b * y_b
            iv = cr * a

            print *, cr, a, iv

        end program

