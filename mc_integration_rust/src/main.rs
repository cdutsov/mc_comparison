use std::f64;
extern crate rand;
use rand::{Rng, SeedableRng};
use rand::rngs::SmallRng;
use std::env;

fn main() {
    let n;
    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        n = args[1].parse::<i32>().unwrap();
    } else {
        println!("Enter number of iterations!");
        return;
    }

    let x_a: f64 = 0.0;
    let x_b: f64 = f64::consts::PI;
    let y_a: f64 = 0.0;
    let y_b: f64 = 1.0;

    let mut count: i32 = 0;

    let mut rand = SmallRng::from_entropy();

    for _i in 0..n {
        let x: f64 = rand.gen_range(x_a, x_b);
        let y: f64 = rand.gen_range(y_a, y_b);

        if y < f64::sin(x) {
            count += 1;
        }
    }

    let count_ratio: f64 = count as f64 / n as f64;
    let area: f64 = x_b * y_b;
    let integral_value: f64 = count_ratio * area;

    println!("Total iterations: {}", n);
    println!("Values under curve: {}", count);
    println!("Integral value: {}", integral_value);
}
