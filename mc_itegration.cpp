#include "stdio.h"
#include "iostream"
#include "math.h"
#include "cstdlib"
#include "random"
using namespace std;

double rng() {
    return (float)rand() / (float)RAND_MAX;
}

inline double scaled_rng(double min, double max) {
    return min + (max - min)*rng();
}

int main(int argc, const char * argv[]) {
    int n = 0;
    if (argc > 1) {
        n = atoi(argv[1]);
    } else {
        return 1;
    }

    float x_a = 0.0;
    float x_b = M_PI;
    float y_a = 0.0;
    float y_b = 1.0;
    int count = 0;

    for(int i = 1; i < n ; i++) {
        double x = scaled_rng(x_a, x_b);
        double y = scaled_rng(y_a, y_b);

        if (y < sin(x)) {
            count += 1;
        }
    }
    double integral_value = count*M_PI/n;
    cout << "Total iterations: " << n << endl;
    cout << "Values under curve: " << count << endl;
    cout << "Integral value: " << integral_value << endl;
}
